A search/matching package that doesn't have to be exact but finds the correct result anyways.

Helps computers finally work for humans, instead of the usual paradigm


## Smart text matching:

    In [1]: from intellimatch.match_text import compare_pattern_similarity, get_closest_match

    In [2]: print(compare_pattern_similarity('cow', 'cowsay'))
    0.6666666666666666

    In [3]: print(compare_pattern_similarity('cow', 'cows'))
    0.8571428571428571

    In [4]: print(compare_pattern_similarity('cow', 'dolphin'))
    0.2

    In [5]: print(compare_pattern_similarity('cow', 'bat'))
    0.0

    In [6]: print(get_closest_match('cow', ['giraffe', 'cows', 'cowsay']))
    cows

    In [7]: print(get_closest_match('dolphin', ['delphi', 'dolls', 'elephants', 'giraffe']))
    delphi

## Smart matching of text input to datetimes

Takes many formats

    "2016-01-23"
    "2016/01/23"
    "01-23-2016"
    "01/23/2016"
    "01-23-16"
    "01/23/16"
    "Jan 23, 2016"
    "Jan 23 2016"
    "January 23, 2016"
    "January 23 2016"
    "23 January 2016"
    "23 January, 2016"

All returned values are datetimes:

    In [1]: from intellimatch.match_datetimes import pad_all_single_digits_with_zero, text_to_datetime

    In [2]: print(pad_all_single_digits_with_zero('3-4-2016 10:00'))
    03-04-2016 10:00

    In [3]: print(text_to_datetime('3-4-2016'))
    2016-03-04 00:00:00

    In [4]: print(text_to_datetime('2016/3/4 10:00'))
    2016-03-04 10:00:00

    In [5]: print(text_to_datetime('03-04-2017 6:00'))
    2017-03-04 06:00:00

    In [6]: print(text_to_datetime('Jan 12, 2015 2:00'))
    2015-01-12 02:00:00

    In [7]: print(text_to_datetime('oops'))
    None
    
    In [8]: what_type_is_it = text_to_datetime('January 23 2015')

    In [9]: type(what_type_is_it)
    Out[9]: datetime.datetime
