from setuptools import setup

setup(
    name='intellimatch',
    version='0.2.0',
    description='Intelligent matching/textsearch/cli interactivity that works for humans',
    url='https://bitbucket.org/codyc54321/intellimatch',
    author='Cody Childers',
    author_email='cchilder@mail.usf.edu',
    license='MIT',
    packages=['intellimatch'],
    install_requires=[],
    zip_safe=False,
)
