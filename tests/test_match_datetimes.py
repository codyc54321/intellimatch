from datetime import datetime
from unittest import TestCase, skip

from tests import CustomTestBase
from intellimatch.match_datetimes import (
    pad_all_single_digits_with_zero,
    generate_acceptable_formats,
    text_to_datetime,
)


class MatchDatetimesTests(CustomTestBase):

    def test_pad_all_single_digits_with_zero(self):
        tests = [
            ('3-4-2016 10:00', '03-04-2016 10:00'),
            ('2016/3/4 10:00', '2016/03/04 10:00'),
            ('3-4-2016', '03-04-2016'),
            ('5', '05'),
            ('Jan 12, 2016', 'Jan 12, 2016'),
        ]
        self.run_assert_equals(pad_all_single_digits_with_zero, tests)

    def test_generate_acceptable_formats(self):
        # make cleaner and more maintainable by autogenerate
        expectation = [
            '%Y-%m-%d', '%Y-%m-%d %H:%M', '%Y-%m-%d %H:%M:%S',
            '%Y/%m/%d', '%Y/%m/%d %H:%M', '%Y/%m/%d %H:%M:%S',
            '%m-%d-%Y', '%m-%d-%Y %H:%M', '%m-%d-%Y %H:%M:%S',
            '%m/%d/%Y', '%m/%d/%Y %H:%M', '%m/%d/%Y %H:%M:%S',
            '%m-%d-%y', '%m-%d-%y %H:%M', '%m-%d-%y %H:%M:%S',
            '%m/%d/%y', '%m/%d/%y %H:%M', '%m/%d/%y %H:%M:%S',
            '%b %d, %Y', '%b %d, %Y %H:%M', '%b %d, %Y %H:%M:%S', # like: "Jan 23, 2016"
            '%b %d %Y', '%b %d %Y %H:%M', '%b %d %Y %H:%M:%S', # like: "Jan 23 2016"
            '%B %d, %Y', '%B %d, %Y %H:%M', '%B %d, %Y %H:%M:%S', # like: "January 23, 2016"
            '%B %d %Y', '%B %d %Y %H:%M', '%B %d %Y %H:%M:%S', # like: "January 23 2016"
            '%d %B %Y', '%d %B %Y %H:%M', '%d %B %Y %H:%M:%S', # like: "23 January 2016"
            '%d %B, %Y', '%d %B, %Y %H:%M', '%d %B, %Y %H:%M:%S', # like: "23 January, 2016"
        ]
        result = generate_acceptable_formats()
        self.assertEqual(result, expectation)


    def test_text_to_datetime(self):
        tests = [
            ('3-4-2016', datetime(2016, 3, 4, 0, 0)),
            ('2016/3/4 10:00', datetime(2016, 3, 4, 10, 0)),
            ('03-04-2017 6:00', datetime(2017, 3, 4, 6, 0)),
            ('Jan 12, 2015 2:00', datetime(2015, 1, 12, 2, 0)),
            ('whoopsies', None),
        ]
        self.run_assert_equals(text_to_datetime, tests)
